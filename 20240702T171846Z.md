To split a pane horizontally in tmux, you can use the following key combination:

Ctrl + b, %

This will split the current pane into two horizontal panes.
# Tue  2 Jul 17:18:46 CEST 2024 - tmux split pane horizontally